#!/bin/sh
. /opt/poky-lsb/2.7/environment-setup-cortexa9t2hf-neon-poky-linux-gnueabi 

# export PATH=/opt/Xilinx/SDK/2017.4/gnu/aarch32/lin/gcc-arm-linux-gnueabi/bin:$PATH
# export CROSS_COMPILE=arm-linux-gnueabihf-
# export ARCH=arm

make distclean
make zynq_psi_wavedaq_dcb_defconfig
make -j 8
